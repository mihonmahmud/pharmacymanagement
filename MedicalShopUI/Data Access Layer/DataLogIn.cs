﻿using System;
using System.Data.SqlClient;
using System.Data;
using MedicalShopUI.Helper;

namespace MedicalShopUI.Data_Access_Layer
{
    public class DataLogIn
    {
        private readonly SqlConnection con;

        public DataLogIn()
        {
            con = new SqlConnection(ConnectString.CnnVal());
        }

        public int GetStatusData(string ids, string pass)
        {
            con.Open();
            string query = string.Format("SELECT * FROM login WHERE user_id='{0}' and password='{1}'", ids, pass);
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr=cmd.ExecuteReader();
                       
            if (dr.HasRows)
            {
                con.Close();
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                int status = int.Parse(dt.Rows[0][3].ToString());

                if (status == 1)
                {
                    con.Close();
                    return 1;
                }
                else
                {
                    con.Close();
                    return 2;
                }           
            }
            else
            {
                con.Close();
                return -1;
            }
        }

        public string GetNameData(string userId)
        {
            con.Open();
            string query = string.Format("SELECT name FROM staffs WHERE user_id='{0}'", userId);
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            string id = dt.Rows[0][0].ToString();

            con.Close();
            return id;
        }

        public int GetIdStatusData(string ids)
        {
            con.Open();
            string query = string.Format("SELECT * FROM login WHERE user_id='{0}'", ids);
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                con.Close();
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                int status = int.Parse(dt.Rows[0][3].ToString());

                if (status == 1)
                {
                    con.Close();
                    return 1;
                }
                else
                {
                    con.Close();
                    return 2;
                }

            }
            else
            {
                con.Close();
                return -1;
            }
        }

        public int GetConfirmData(string id, string email)
        {
            con.Open();
            string query = string.Format("SELECT email FROM staffs WHERE user_id='{0}'", id);
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            string uEmail = dt.Rows[0][0].ToString();

            if (uEmail == email)
            {
                con.Close();
                return 1;
            }
            else
            {
                con.Close();
                return 0;
            }
        }
    }
}
