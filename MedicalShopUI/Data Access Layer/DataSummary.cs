﻿using MedicalShopUI.Helper;
using System.Data;
using System.Data.SqlClient;

namespace MedicalShopUI.Data_Access_Layer
{
    public class DataSummary
    {
        private static readonly SqlConnection con = new SqlConnection(ConnectString.CnnVal());

        public static DataTable GetCategoryCount()
        {
            con.Open();
            string query = string.Format("SELECT category, COUNT(*) AS cat_count FROM medicines GROUP BY category ORDER BY category ASC");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public static DataTable GetCompanyCount()
        {
            con.Open();
            string query = string.Format("select c.company_name as c_name, count(*) as c_count from companys c, medicines m where c.company_id=m.company_id group by c.company_name order by c.company_name");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }



        public static DataTable GetsaleCount()
        {
            con.Open();
            string query = string.Format("select m.category as category, count(*) as m_count from medicines m, sales s where s.medicine_name=m.medicine_name group by m.category order by m.category");
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }
    }
}
